<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base.jsp"%>

		<spring:url value="/resources/css/corsi.css" var="corsiCSS" />
		<link href="${corsiCSS}" rel="stylesheet" />
	</head>
	<body>
		
		<div class="page">
			<jsp:include page="./sideBar/sideBar.jsp" />
			
			<div class="header_content_container" id="lista">
				<jsp:include page="./header/header.jsp" >
					<jsp:param name="title" value="Corsi" />
					<jsp:param name="icon" value="${bookPlusSVG}" />
					<jsp:param name="text_button" value="Aggiungi corso" />
					<jsp:param name="link" value="insert-corso" />
				</jsp:include>

			</div>
		</div>
	
	</body>
	<script>
		var container = document.getElementById("lista");
		var len = "${len}";
		
		if(len > 0) {

			container.innerHTML += `
				<jsp:include page="./contentView/contentViewGrid.jsp" />
			`;

			var lista = document.getElementById("grid");

			<c:forEach var="corso" items="${corsi}">
				lista.innerHTML += `
					<jsp:include page="./card/card_corsi.jsp" >
						<jsp:param name="title" value="${corso.nomeC}" />
						<jsp:param name="icon" value="${bookSVG}" />
						<jsp:param name="cfu" value="Peso ${corso.CFU} cfu" />
						<jsp:param name="area" value="Settore ${corso.settoreC}" />
						<jsp:param name="link" value="view-corso/${corso.idC}" />
					</jsp:include>
				`;
			</c:forEach>
		} else {
			container.innerHTML += `
				<jsp:include page="./contentView/contentViewBlank.jsp" >
					<jsp:param name="title" value="Nessun corso trovato" />
					<jsp:param name="text" value="Cerca in alto a destra o aggiungi un nuovo corso." />
				</jsp:include>
			`;
		}
	</script>
</html>