
<form action="${param.link}" method="GET" id="formSearch">
    <div class="searchbar">
        
        <input type="text" id="corsoStudi" name="corsoStudi" value="" placeholder="Scegli il corso di laurea" required/>
        <input type="text" id="tipo" name="tipo" value="" placeholder="Scegli il tipo" required/>
        <input type="number" id="anno" name="anno" value="" placeholder="Scegli l'anno" required/>
        
        <div class="button">
            <div class="cerca">
                <input class="submit" id="cerca" type="submit" value="Cerca"/>
            </div>
        </div>
    </div>
</form>