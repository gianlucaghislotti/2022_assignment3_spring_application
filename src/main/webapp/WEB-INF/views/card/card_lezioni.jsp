
<form id="${param.title}${param.room}${param.hour}${param.type}${param.day}" action="${param.link}" method="GET">
    <div class="card" onclick='document.getElementById("${param.title}${param.room}${param.hour}${param.type}${param.day}").submit();'>
        <div class="container">
            <img src="${param.icon}">
            <div class="text_container">
                <h1 class="reduct">${param.title}</h1>
                <p>${param.room}</p>
                <p id="blue">${param.hour}</p>
                <p>${param.type}</p>
            </div>
        </div>
    </div>
</form>