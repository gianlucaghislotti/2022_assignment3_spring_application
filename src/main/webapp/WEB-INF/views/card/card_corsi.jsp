
<form id="${param.title}${param.cfu}${param.area}" action="${param.link}" method="GET">
    <div class="card" onclick='document.getElementById("${param.title}${param.cfu}${param.area}").submit();'>
        <div class="container">
            <img src="${param.icon}">
            <div class="text_container">
                <h1 class="reduct">${param.title}</h1>
                <p>${param.cfu}</p>
                <p id="blue">${param.area}</p>
            </div>
        </div>
    </div>
</form>