
<form id="${param.title}" action="${param.link}" method="GET">
    <div class="card" onclick='document.getElementById("${param.title}").submit();'>
        <div class="container">
            <img src="${param.icon}">
            <div class="text_container">
                <h1>${param.title}</h1>
                <p>${param.seats}</p>
            </div>
        </div>
    </div>
</form>