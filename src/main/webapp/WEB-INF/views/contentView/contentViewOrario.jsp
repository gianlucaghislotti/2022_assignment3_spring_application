
<div class="contentView" id="orario">
    <jsp:include page="./day_column.jsp" >
        <jsp:param name="title" value="Lunedì" />
    </jsp:include>

    <div class="divider"></div>

    <jsp:include page="./day_column.jsp" >
        <jsp:param name="title" value="Martedì" />
    </jsp:include>

    <div class="divider"></div>

    <jsp:include page="./day_column.jsp" >
        <jsp:param name="title" value="Mercoledì" />
    </jsp:include>

    <div class="divider"></div>

    <jsp:include page="./day_column.jsp" >
        <jsp:param name="title" value="Giovedì" />
    </jsp:include>

    <div class="divider"></div>

    <jsp:include page="./day_column.jsp" >
        <jsp:param name="title" value="Venerdì" />
    </jsp:include>
</div>