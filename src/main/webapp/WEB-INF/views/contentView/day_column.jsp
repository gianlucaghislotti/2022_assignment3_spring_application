<%@include file="../base.jsp"%>

<div class="day">
    <div class="container_title">
        <h1>${param.title}</h1>
    </div>
    <div class="lista">
        <jsp:include page="../card/card_lezioni.jsp" >
            <jsp:param name="title" value="Teoria della Computazione" />
            <jsp:param name="icon" value="${clockSVG}" />
            <jsp:param name="room" value="Aula U1-09" />
            <jsp:param name="hour" value="Ore 11:30 - 13:00" />
            <jsp:param name="type" value="Lezione teorica" />
            <jsp:param name="day" value="Luned�" />
        </jsp:include>

        <jsp:include page="../card/card_lezioni.jsp" >
            <jsp:param name="title" value="Teoria della Computazione" />
            <jsp:param name="icon" value="${clockSVG}" />
            <jsp:param name="room" value="Aula U1-09" />
            <jsp:param name="hour" value="Ore 11:30 - 13:00" />
            <jsp:param name="type" value="Lezione teorica" />
            <jsp:param name="day" value="Luned�" />
        </jsp:include>

        <jsp:include page="../card/card_lezioni.jsp" >
            <jsp:param name="title" value="Teoria della Computazione" />
            <jsp:param name="icon" value="${clockSVG}" />
            <jsp:param name="room" value="Aula U1-09" />
            <jsp:param name="hour" value="Ore 11:30 - 13:00" />
            <jsp:param name="type" value="Lezione teorica" />
            <jsp:param name="day" value="Luned�" />
        </jsp:include>
    </div>
</div>