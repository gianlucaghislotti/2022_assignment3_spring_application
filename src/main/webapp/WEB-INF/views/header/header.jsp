
<div class="header" id="header">
    <div class="container">
        
        <jsp:include page="../button/selection_hug.jsp" >
            <jsp:param name="icon" value="${param.icon}" />
            <jsp:param name="text" value="${param.text_button}" />
            <jsp:param name="link" value="${param.link}" />
        </jsp:include>

        <div class="title_container">
            <h1>${param.title}</h1>
        </div>

    </div>
</div>