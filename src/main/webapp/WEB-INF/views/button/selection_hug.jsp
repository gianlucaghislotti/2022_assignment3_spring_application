
<form id="${param.link}" action="${param.link}" method="GET">
	<div class="button" onclick='document.getElementById("${param.link}").submit();'>
	    <div class="select" id="small">
	        <div class="container">
	            <img src="${param.icon}" />
	            <p>${param.text}</p>
	        </div>
	    </div>
	</div>
</form>