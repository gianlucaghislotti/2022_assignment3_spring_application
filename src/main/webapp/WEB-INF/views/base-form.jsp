
<!-- Import per ogni pagina JSP -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- Import per i fogli di stile CSS -->
<spring:url value="/resources/css/basic.css" var="basicCSS" />
<link href="${basicCSS}" rel="stylesheet" />
<spring:url value="/resources/css/button.css" var="buttonCSS" />
<link href="${buttonCSS}" rel="stylesheet" />
<spring:url value="/resources/css/form.css" var="formCSS" />
<link href="${formCSS}" rel="stylesheet" />

<!-- Variabili per importare gli assets -->
<spring:url value="/resources/assets/book.svg" var="bookSVG" />
<link href="${bookSVG}" rel="icon" />
<spring:url value="/resources/assets/clock.svg" var="clockSVG" />
<link href="${clockSVG}" rel="icon" />
<spring:url value="/resources/assets/aule.svg" var="auleSVG" />
<link href="${auleSVG}" rel="icon" />
<spring:url value="/resources/assets/bookPlus.svg" var="bookPlusSVG" />
<link href="${bookPlusSVG}" rel="icon" />
<spring:url value="/resources/assets/clockPlus.svg" var="clockPlusSVG" />
<link href="${clockPlusSVG}" rel="icon" />
<spring:url value="/resources/assets/aulePlus.svg" var="aulePlusSVG" />
<link href="${aulePlusSVG}" rel="icon" />
