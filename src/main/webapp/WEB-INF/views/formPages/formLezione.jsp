
<form action="${param.link}" method="POST" id="form">
    <div class="form">
        <jsp:include page="./formTitle.jsp">
            <jsp:param name="action" value="${param.action}" />
            <jsp:param name="page" value="${param.page}" />
        </jsp:include>

        <div class="inputs" id="inputs">
            <jsp:include page="../form/input_select.jsp" >
                <jsp:param name="label" value="codC" />
                <jsp:param name="text" value="CORSO" />
            </jsp:include>  

            <jsp:include page="../form/input_select.jsp" >
                <jsp:param name="label" value="codA" />
                <jsp:param name="text" value="AULA" />
            </jsp:include>

            <jsp:include page="../form/5_radio_button.jsp" >
                <jsp:param name="label" value="dataL" />
                <jsp:param name="text" value="GIORNO" />
                <jsp:param name="choice1" value="Lun" />
                <jsp:param name="choice2" value="Mar" />
                <jsp:param name="choice3" value="Mer" />
                <jsp:param name="choice4" value="Gio" />
                <jsp:param name="choice5" value="Ven" />
            </jsp:include>

            <jsp:include page="../form/horizontal_input.jsp" >
                <jsp:param name="label1" value="ora_inizioL" />
                <jsp:param name="text1" value="ORA DI INIZIO" />
                <jsp:param name="label2" value="ora_fineL" />
                <jsp:param name="text2" value="ORA DI FINE" />
            </jsp:include>
            
            <jsp:include page="../form/2_radio_button.jsp" >
                <jsp:param name="label" value="tipoL" />
                <jsp:param name="text" value="TIPO" />
                <jsp:param name="choice1" value="Teoria" />
                <jsp:param name="choice2" value="Laboratorio" />
            </jsp:include>
            
            <jsp:include page="../form/input_textarea.jsp" >
                <jsp:param name="label" value="descrizioneL" />
                <jsp:param name="text" value="DESCRIZIONE" />
            </jsp:include>
        </div>

        <div class="button">
            <div class="submit">
                <input class="submit" type="submit" value="${param.button}" />
            </div>
        </div>
    </div>
</form>
