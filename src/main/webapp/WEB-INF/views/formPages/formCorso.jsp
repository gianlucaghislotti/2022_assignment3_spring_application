
<form action="${param.link}" method="POST" id="form">
    <div class="form">
        <jsp:include page="./formTitle.jsp">
            <jsp:param name="action" value="${param.action}" />
            <jsp:param name="page" value="${param.page}" />
        </jsp:include>

        <div class="inputs">
            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="nomeC" />
                <jsp:param name="text" value="NOME" />
            </jsp:include>

            <jsp:include page="../form/2_radio_button.jsp" >
                <jsp:param name="label" value="tipoC" />
                <jsp:param name="text" value="TIPO DI LAUREA" />
                <jsp:param name="choice1" value="Triennale" />
                <jsp:param name="choice2" value="Magistrale" />
            </jsp:include>

            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="area_didatticaC" />
                <jsp:param name="text" value="AREA DIDATTICA" />
            </jsp:include>

            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="corso_studioC" />
                <jsp:param name="text" value="CORSO DI STUDI" />
            </jsp:include>

            <jsp:include page="../form/3_radio_button.jsp" >
                <jsp:param name="label" value="annoC" />
                <jsp:param name="text" value="ANNO" />
                <jsp:param name="choice1" value="1" />
                <jsp:param name="choice2" value="2" />
                <jsp:param name="choice3" value="3" />
            </jsp:include>

            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="linguaC" />
                <jsp:param name="text" value="LINGUA" />
            </jsp:include>

            <jsp:include page="../form/2_radio_button.jsp" >
                <jsp:param name="label" value="attivitaC" />
                <jsp:param name="text" value="ATTIVITÀ" />
                <jsp:param name="choice1" value="Obbligatoria" />
                <jsp:param name="choice2" value="A scelta" />
            </jsp:include>

            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="settoreC" />
                <jsp:param name="text" value="SETTORE" />
            </jsp:include>

            <jsp:include page="../form/2_radio_button.jsp" >
                <jsp:param name="label" value="semestreC" />
                <jsp:param name="text" value="SEMESTRE" />
                <jsp:param name="choice1" value="1" />
                <jsp:param name="choice2" value="2" />
            </jsp:include>

            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="CFU" />
                <jsp:param name="text" value="CFU" />
            </jsp:include>
        </div>

        <div class="button">
            <div class="submit">
                <input class="submit" type="submit" value="${param.button}" />
            </div>
        </div>
    </div>
</form>
