
<form action="${param.link}" method="POST" id="form">
    <div class="form">
        <jsp:include page="./formTitle.jsp">
            <jsp:param name="action" value="${param.action}" />
            <jsp:param name="page" value="${param.page}" />
        </jsp:include>

        <div class="inputs">
            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="nomeA" />
                <jsp:param name="text" value="NOME" />
            </jsp:include>
            <jsp:include page="../form/input.jsp" >
                <jsp:param name="label" value="postiA" />
                <jsp:param name="text" value="NUMERO DI POSTI" />
            </jsp:include>
        </div>

        <div class="button">
            <div class="submit">
                <input class="submit" type="submit" value="${param.button}" />
            </div>
        </div>
    </div>
</form>
