<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base-form.jsp"%>
	</head>
	<body>

    </body>
    <script>

        var body = document.getElementsByTagName('body')[0];
        var len = "${len}";

        body.innerHTML += `
            <jsp:include page="./formPages/formCorso.jsp" >
                <jsp:param name="action" value="Aggiungi" />
                <jsp:param name="page" value="Corso" />
                <jsp:param name="link" value="add-corso" />
                <jsp:param name="button" value="Salva" />
            </jsp:include>
        `;

        var form = document.getElementById('form');
        form.addEventListener("submit", (e) => {
            check(e);
            createLink();
        });

        if(len > 0) {
            var inputs = document.getElementsByClassName('inputs')[0];
            
            inputs.innerHTML += `
                <jsp:include page="./form/dynamic_checkbox.jsp" >
                    <jsp:param name="label" value="propedeutici" />
                    <jsp:param name="text" value="CORSI PROPEDEUTICI" />
                </jsp:include>
            `;
            
            <c:forEach var="corso" items="${corsi}">
                inputs.lastElementChild.lastElementChild.innerHTML += `
                    <jsp:include page="./form/checkbox.jsp" >
                        <jsp:param name="label" value="propedeutici" />
                        <jsp:param name="choice" value="${corso.nomeC}" />
                        <jsp:param name="type" value="checkbox" />
                        <jsp:param name="id" value="${corso.idC}" />
                    </jsp:include>
                `;
            </c:forEach>

        }

        document.getElementsByClassName('form')[0].addEventListener("click", 
        
        function handlerAnno() {
            var magistrale_radio = document.getElementById('MagistraletipoC');
            var anno3 = document.getElementById('3annoC');
            
            if(magistrale_radio.checked) {
                if(anno3 != null) {
                    var child = anno3.parentNode;
                    child.parentNode.removeChild(child);
                }
            } else if(anno3 == null) {
                var parent = document.getElementById('2annoC').parentNode.parentNode;
                parent.innerHTML += `
                
                    <div class="choice">
                        <input type="radio" name="annoC" id="3annoC" value="3" />
                        <label for="3annoC">3</label>
                    </div>
                
                `;
            }
        });

        function check(e) {
            var cfu = document.getElementById('CFU');

            if(isNaN(cfu.value)) {
                alert("Il campo CFU deve essere numerico");
                e.preventDefault();
            } 

            var nome = document.getElementById('nomeC').value;

            <c:forEach var="corso" items="${corsi}">
                checkNames(nome, "${corso.nomeC}", e);
            </c:forEach>

        }

        function createLink() {

            var nome = document.getElementById('nomeC').value;

            var magistrale = document.getElementById('MagistraletipoC');
            var triennale = document.getElementById('TriennaletipoC');
            var tipo = magistrale.checked ? magistrale.value : triennale.value;

            var anno3 = document.getElementById('3annoC');
            var anno2 = document.getElementById('2annoC');
            var anno1 = document.getElementById('1annoC');
            
            if(anno3 == null) {
                var anno = anno2.checked ? anno2.value : anno1.value;
            } else {
                var anno = anno3.checked ? anno3.value : anno2.checked ? anno2.value : anno1.value;
            }

            var obbligatoria = document.getElementById('ObbligatoriaattivitaC');
            var ascelta = document.getElementById('A sceltaattivitaC');
            var attività = obbligatoria.checked ? obbligatoria.value : ascelta.value;

            var sem1 = document.getElementById('1semestreC');
            var sem2 = document.getElementById('2semestreC');
            var semestre = sem1.checked ? sem1.value : sem2.value;

            var area = document.getElementById('area_didatticaC').value;
            var corso = document.getElementById('corso_studioC').value;
            var lingua = document.getElementById('linguaC').value;
            var settore = document.getElementById('settoreC').value;
            var cfu = document.getElementById('CFU').value;

            var link = "add-corso";
            link += "?nome=" + nome;
            link += "&tipo=" + tipo;
            link += "&area=" + area;
            link += "&corso=" + corso;
            link += "&anno=" + anno;
            link += "&lingua=" + lingua;
            link += "&attivita=" + attività;
            link += "&settore=" + settore;
            link += "&semestre=" + semestre;
            link += "&cfu=" + cfu;
            link += "&propedeutici=";
            var inputs = document.getElementsByClassName('inputs')[0];
            var radios = inputs.lastElementChild.lastElementChild.children;
            var form = document.getElementById('form');

            for(var i=0; i<radios.length; i++) {
                var checkbox = radios[i].children[0];
                if(checkbox.checked) {
                    link += "" + checkbox.id;
                    link += ",";
                }
            }

            link = link.substring(0, link.length - 1);
            form.action = link;
            var child = inputs.lastElementChild;
            child.parentNode.removeChild(child);
        }

        function checkNames(name1, name2, e) {
            if(name1.toLowerCase() == name2.toLowerCase()) {
                alert("Esiste un corso con lo stesso nome, cambialo e riprova!!");
                e.preventDefault();
            }
        }

    </script>
</html>
