<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base-form.jsp"%>
	</head>
	<body>

        <jsp:include page="./formPages/formAula.jsp" >
            <jsp:param name="action" value="Aggiungi" />
            <jsp:param name="page" value="Aula" />
            <jsp:param name="link" value="add-aula" />
            <jsp:param name="button" value="Salva" />
        </jsp:include> 
        
    </body>
    <script>
        var form = document.getElementById('form');
        form.addEventListener("submit", (e) => {
            var posti = document.getElementById('postiA');

            if(isNaN(posti.value)) {
                alert("Il campo 'numero di posti' deve essere numerico");
                e.preventDefault();
            }
        });

    </script>
</html>
