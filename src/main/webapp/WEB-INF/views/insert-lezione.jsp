<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base-form.jsp"%>
	</head>
	<body>

        <jsp:include page="./formPages/formLezione.jsp" >
            <jsp:param name="action" value="Aggiungi" />
            <jsp:param name="page" value="Lezione" />
            <jsp:param name="link" value="" />
            <jsp:param name="button" value="Salva" />
        </jsp:include>

    </body> 
    <script>

        var selectCorsi = document.getElementById('codC');
        var selectAule = document.getElementById('codA');

        var form = document.getElementById('form');
        form.addEventListener("submit", (e) => {
            createLink();
            check(e);
        });
        
        selectAule.innerHTML += `
            <option disabled selected value="0">Clicca qui per inserire l'aula</option>
            `;

        <c:forEach var="aula" items="${aule}">
            selectAule.innerHTML += `
                <option value="${aula.idA}" required>${aula.nomeA}</option>
            `;
        </c:forEach>

        selectCorsi.innerHTML += `
            <option disabled selected value="0">Clicca qui per inserire il corso</option>
            `;

        <c:forEach var="corso" items="${corsi}">
            selectCorsi.innerHTML += `
                <option value="${corso.idC}" required>${corso.nomeC}</option>
            `;
        </c:forEach>

        var inputs = document.getElementById('inputs');
        inputs.innerHTML += `
            <div style="gap: 20px;display: flex;flex-direction: column;align-items: flex-start;width: inherit;">
            </div>
        `;

        document.getElementsByClassName('form')[0].addEventListener("click", 
        
        function handlerTipo() {

            var teoria = document.getElementById('TeoriatipoL');
            var laboratorio = document.getElementById('LaboratoriotipoL');
            var tutor = document.getElementById('tutortutor');
            var computer = document.getElementById('computercomputer');
            var inputs = document.getElementById('inputs');

            if(teoria.checked) {
                if(computer != null) {
                    var child = inputs.lastElementChild.lastElementChild;
                    inputs.lastElementChild.removeChild(child);
                    child = inputs.lastElementChild.lastElementChild;
                    inputs.lastElementChild.removeChild(child);
                }
                
                if(tutor == null) {
                    inputs.lastElementChild.innerHTML += `
                        <jsp:include page="./form/input.jsp" >
                            <jsp:param name="label" value="libro_testo" />
                            <jsp:param name="text" value="LIBRO DI TESTO" />
                        </jsp:include>

                        <jsp:include page="./form/dynamic_checkbox.jsp" >
                            <jsp:param name="label" value="tutor" />
                            <jsp:param name="text" value="TUTOR" />
                        </jsp:include>
                    `;
                    inputs.lastElementChild.lastElementChild.lastElementChild.innerHTML += `
                        <jsp:include page="./form/input_radio&checkbox.jsp" >
                            <jsp:param name="label" value="tutor" />
                            <jsp:param name="choice" value="tutor" />
                            <jsp:param name="type" value="checkbox" />
                        </jsp:include>
                    `;
                }
            }

            if(laboratorio.checked) {
                if(tutor != null) {
                    var child = inputs.lastElementChild.lastElementChild;
                    inputs.lastElementChild.removeChild(child);
                    child = inputs.lastElementChild.lastElementChild;
                    inputs.lastElementChild.removeChild(child);
                }
                
                if(computer == null) {
                    inputs.lastElementChild.innerHTML += `
                        <jsp:include page="./form/input.jsp" >
                            <jsp:param name="label" value="materiale" />
                            <jsp:param name="text" value="MATERIALE" />
                        </jsp:include>

                        <jsp:include page="./form/dynamic_checkbox.jsp" >
                            <jsp:param name="label" value="computer" />
                            <jsp:param name="text" value="COMPUTER" />
                        </jsp:include>
                    `;
                    inputs.lastElementChild.lastElementChild.lastElementChild.innerHTML += `
                        <jsp:include page="./form/input_radio&checkbox.jsp" >
                            <jsp:param name="label" value="computer" />
                            <jsp:param name="choice" value="computer" />
                            <jsp:param name="type" value="checkbox" />
                        </jsp:include>
                    `;
                }
            }
        });

        function createLink() {
            var tipo_teoria = document.getElementById('TeoriatipoL');
            var tipo_laboratorio = document.getElementById('LaboratoriotipoL');
            var body = document.getElementsByTagName('body')[0];
            var form = document.getElementById('form');
            var selectCorsi = document.getElementById('codC');
            var selectAule = document.getElementById('codA');

            var tipo = "";

            if(tipo_teoria.checked) {
                tipo = tipo_teoria;
            } else {
                tipo = tipo_laboratorio;
            }

            var oraInizio = document.getElementById('ora_inizioL');
            var oraFine = document.getElementById('ora_fineL');
            var descrizione = document.getElementById('descrizioneL');

            var lun = document.getElementById('LundataL');
            var mar = document.getElementById('MardataL');
            var mer = document.getElementById('MerdataL');
            var gio = document.getElementById('GiodataL');
            var ven = document.getElementById('VendataL');

            var list = [lun, mar, mer, gio, ven];
            var giorno = "";

            list.forEach(day => {
                if(day.checked) {
                    giorno = day.value;
                }
            })

            var libro_testo = document.getElementById('libro_testo');
            var materiale = document.getElementById('materiale');
            var tutor = document.getElementById('tutortutor');
            var computer = document.getElementById('computercomputer');


            var link = "add-lezione";
            link += "?tipo=" + tipo.value.toLowerCase();
            link += "&aula=" + selectAule.value;
            link += "&corso=" + selectCorsi.value;
            link += "&ora_inizio=" + oraInizio.value;
            link += "&ora_fine=" + oraFine.value;
            link += "&descrizione=" + descrizione.value;
            link += "&giorno=" + giorno;

            if(tipo.value == 'Teoria') {
                link += "&tutorT=" + tutor.checked;
                link += "&libro_testoT=" + libro_testo.value;
            } else {
            	link += "&materialeL=" + materiale.value;
                link += "&computerL=" + computer.checked;
                
            }
            form.action = link;
        }

        function check(e) {
            var oraInizio = document.getElementById('ora_inizioL');
            var oraFine = document.getElementById('ora_fineL');
            var selectCorso = document.getElementById('codC');
            var selectAula = document.getElementById('codA');
            var hourEX = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;

            if(selectCorso.value == 0 || selectAula.value == 0) {
                alert("Devi selezionare almeno un corso e una aula");
                e.preventDefault();
            }

            if(!hourEX.test(oraInizio.value) || !hourEX.test(oraFine.value)) {
                alert("L'ora di inizio e di fine della lezione devono essere ore in formato HH:mm");
                e.preventDefault();
            }
        }

    </script>
</html>
