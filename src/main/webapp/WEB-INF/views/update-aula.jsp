<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base-form.jsp"%>
	</head>
	<body> 
        
    </body>
    <script>

        var body = document.getElementsByTagName('body')[0];

        body.innerHTML += `
            <jsp:include page="./formPages/formAula.jsp" >
                <jsp:param name="action" value="Modifica" />
                <jsp:param name="page" value="Aula" />
                <jsp:param name="link" value="/SpringMvcCrud/update-aula/${aula.idA}" />
                <jsp:param name="button" value="Salva" />
            </jsp:include>
        `;

        var parent = document.getElementsByClassName('form')[0];
        var child = parent.lastElementChild;
        parent.removeChild(child);
        parent.innerHTML += `
            <jsp:include page="./button/2_button.jsp" >
                <jsp:param name="primary" value="Salva" />
                <jsp:param name="secondary" value="Elimina" />
            </jsp:include>
        `;

        var nome = document.getElementById('nomeA');
        var posti = document.getElementById('postiA');
        nome.value = "${aula.nomeA}";
        posti.value = "${aula.postiA}";

        document.getElementById('button1').addEventListener('click', (e) => {
            check(e);
        });

        document.getElementById('button2').addEventListener('click', (e) => {
            check(e);

            var link = "/SpringMvcCrud/delete-aula/${aula.idA}";
            var form = document.getElementById('form');
            form.action = link;
        });

        function check(e) {
            var posti = document.getElementById('postiA');

            if(isNaN(posti.value)) {
                alert("Il campo 'numero di posti' deve essere numerico");
                e.preventDefault();
            }
        }

    </script>
</html>
