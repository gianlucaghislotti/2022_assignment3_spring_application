<%@include file="../base.jsp"%>

<div class="sidebar">
     <div class="container-button-title-sideBar">
         <div class="container-title-sideBar">
             <h1>Uni</h1>
             <h1 class="blue">Orario</h1>
         </div>
         <div class="container-button-sideBar">
             
             <jsp:include page="../button/sidebar_selection.jsp" >
				<jsp:param name="icon" value="${auleSVG}" />
				<jsp:param name="text" value="Aule" />
				<jsp:param name="link" value="aule" />
			</jsp:include>
		
		    <jsp:include page="../button/sidebar_selection.jsp" >
				<jsp:param name="icon" value="${bookSVG}" />
				<jsp:param name="text" value="Corsi" />
				<jsp:param name="link" value="corsi" />
			</jsp:include>
		
		    <jsp:include page="../button/sidebar_selection.jsp" >
				<jsp:param name="icon" value="${clockSVG}" />
				<jsp:param name="text" value="Lezioni" />
				<jsp:param name="link" value="lezioni" />
			</jsp:include>
             
    	</div>
	</div>
</div>