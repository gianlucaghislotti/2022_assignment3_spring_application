
<div class="input" id="vertical">
    <div class="label">
        <label for="${param.label}">${param.text}</label>
    </div>
    <textarea id="${param.label}" name="${param.label}" required></textarea>
</div>