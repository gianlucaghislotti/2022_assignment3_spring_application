
<div class="input" id="vertical">
    <div class="label">
        <label for="${param.label}">${param.text}</label>
    </div>
    <div class="radio">
        <jsp:include page="./input_radio&checkbox.jsp" >
            <jsp:param name="label" value="${param.label}" />
            <jsp:param name="choice" value="${param.choice1}" />
            <jsp:param name="type" value="radio" />
        </jsp:include>
        <jsp:include page="./input_radio&checkbox.jsp" >
            <jsp:param name="label" value="${param.label}" />
            <jsp:param name="choice" value="${param.choice2}" />
            <jsp:param name="type" value="radio" />
        </jsp:include>
        <jsp:include page="./input_radio&checkbox.jsp" >
            <jsp:param name="label" value="${param.label}" />
            <jsp:param name="choice" value="${param.choice3}" />
            <jsp:param name="type" value="radio" />
        </jsp:include>
    </div>
</div>