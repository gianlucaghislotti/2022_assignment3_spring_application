
<div class="hinput">
    <div class="input" id="horizontal">
        <div class="label">
            <label for="${param.label1}">${param.text1}</label>
        </div>
        <input type="text" id="${param.label1}" name="${param.label1}" value="" required/>
    </div>
    <div class="input" id="horizontal">
        <div class="label">
            <label for="${param.label2}">${param.text2}</label>
        </div>
        <input type="text" id="${param.label2}" name="${param.label2}" value="" required/>
    </div>
</div>