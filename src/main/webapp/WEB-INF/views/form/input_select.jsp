
<div class="input" id="vertical">
    <div class="label">
        <label for="${param.label}">${param.text}</label>
    </div>
    <select name="${param.label}" class="select" id="${param.label}">
    </select>
</div>