
<div class="input" id="vertical">
    <div class="label">
        <label for="${param.label}">${param.text}</label>
    </div>
    <input type="text" id="${param.label}" name="${param.label}" value="" required/>
</div>