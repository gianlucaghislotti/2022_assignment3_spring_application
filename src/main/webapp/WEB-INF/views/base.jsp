
<!-- Import per ogni pagina JSP -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- Import per i fogli di stile CSS -->
<spring:url value="/resources/css/basic.css" var="basicCSS" />
<link href="${basicCSS}" rel="stylesheet" />
<spring:url value="/resources/css/button.css" var="buttonCSS" />
<link href="${buttonCSS}" rel="stylesheet" />
<spring:url value="/resources/css/searchBar.css" var="searchBarCSS" />
<link href="${searchBarCSS}" rel="stylesheet" />
<spring:url value="/resources/css/sideBar.css" var="sideBarCSS" />
<link href="${sideBarCSS}" rel="stylesheet" />
<spring:url value="/resources/css/header.css" var="headerCSS" />
<link href="${headerCSS}" rel="stylesheet" />
<spring:url value="/resources/css/contentView.css" var="contentViewCSS" />
<link href="${contentViewCSS}" rel="stylesheet" />
<spring:url value="/resources/css/columnDay.css" var="columnDayCSS" />
<link href="${columnDayCSS}" rel="stylesheet" />
<spring:url value="/resources/css/page.css" var="pageCSS" />
<link href="${pageCSS}" rel="stylesheet" />
<spring:url value="/resources/css/card.css" var="cardCSS" />
<link href="${cardCSS}" rel="stylesheet" />

<!-- Variabili per importare gli assets -->
<spring:url value="/resources/assets/book.svg" var="bookSVG" />
<link href="${bookSVG}" rel="icon" />
<spring:url value="/resources/assets/clock.svg" var="clockSVG" />
<link href="${clockSVG}" rel="icon" />
<spring:url value="/resources/assets/aule.svg" var="auleSVG" />
<link href="${auleSVG}" rel="icon" />
<spring:url value="/resources/assets/bookPlus.svg" var="bookPlusSVG" />
<link href="${bookPlusSVG}" rel="icon" />
<spring:url value="/resources/assets/clockPlus.svg" var="clockPlusSVG" />
<link href="${clockPlusSVG}" rel="icon" />
<spring:url value="/resources/assets/aulePlus.svg" var="aulePlusSVG" />
<link href="${aulePlusSVG}" rel="icon" />
