<!DOCTYPE HTML>
<html>
	<head> 
		<link href="${clockSVG}" rel="icon" />
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base.jsp"%>

		<spring:url value="/resources/css/aule.css" var="auleCSS" />
		<link href="${auleCSS}" rel="stylesheet" />
	</head>
	<body>

		<div class="page">
			<jsp:include page="./sideBar/sideBar.jsp" />
			
			<div class="header_content_container" id="lista">
				<jsp:include page="./header/header.jsp" >
					<jsp:param name="title" value="Aule" />
					<jsp:param name="icon" value="${aulePlusSVG}" />
					<jsp:param name="text_button" value="Aggiungi aula" />
					<jsp:param name="link" value="insert-aula" />
				</jsp:include>

			</div>
		</div>
		
	</body>
	<script>
		var container = document.getElementById("lista");
		var len = "${len}";
		
		if(len > 0) {

			container.innerHTML += `
				<jsp:include page="./contentView/contentViewGrid.jsp" />
			`;

			var lista = document.getElementById("grid");

			<c:forEach var="aula" items="${aule}">
				lista.innerHTML += `
					<jsp:include page="./card/card_aule.jsp" >
						<jsp:param name="title" value="${aula.nomeA}" />
						<jsp:param name="icon" value="${auleSVG}" />
						<jsp:param name="seats" value="Capacità ${aula.postiA} posti" />
						<jsp:param name="link" value="view-aula/${aula.idA}" />
					</jsp:include>
				`;
			</c:forEach>
		} else {
			container.innerHTML += `
				<jsp:include page="./contentView/contentViewBlank.jsp" >
					<jsp:param name="title" value="Nessuna aula trovata" />
					<jsp:param name="text" value="Cerca in alto a destra o aggiungi una nuova aula." />
				</jsp:include>
			`;
		}
	</script>
</html>