<!DOCTYPE HTML>
<html>
	<head> 
    	<title>Uni Orario</title>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    	<%@include file="./base.jsp"%>

		<spring:url value="/resources/css/lezioni.css" var="lezioniCSS" />
		<link href="${lezioniCSS}" rel="stylesheet" />
	</head>
	<body>
		
		<div class="page">
			<jsp:include page="./sideBar/sideBar.jsp" />
			
			<div class="header_content_container" id="lista">
				<jsp:include page="./header/header.jsp" >
					<jsp:param name="title" value="Lezioni" />
					<jsp:param name="icon" value="${clockPlusSVG}" />
					<jsp:param name="text_button" value="Aggiungi lezione" />
					<jsp:param name="link" value="insert-lezione" />
				</jsp:include>

			</div>
		</div>
	
	</body>
	<script>
		var container = document.getElementById("lista");
		const len = "${len}";
		const lenAule = "${lenAule}";
		const lenCorsi = "${lenCorsi}";

		var title = "";
		var text = "";
		
		if(len > 0) {

			container.innerHTML += `
				<jsp:include page="./contentView/contentViewGrid.jsp" />
			`;

			var lista = document.getElementById("grid");
			var tipo = "";

			<c:forEach var="lezione" items="${lezioni}">
				tipo = ((("${lezione}".split("."))[3]).split("@"))[0];
				lista.innerHTML += `
					<jsp:include page="./card/card_lezioni.jsp" >
						<jsp:param name="title" value="${lezione.codC.nomeC}" />
						<jsp:param name="icon" value="${clockSVG}" />
						<jsp:param name="room" value="Aula ${lezione.codA.nomeA}" />
						<jsp:param name="hour" value="Ore ${lezione.ora_inizioL} - ${lezione.ora_fineL}" />
						<jsp:param name="type" value="`+tipo+`" />
						<jsp:param name="link" value="view-lezione/${lezione.idL}" />
					</jsp:include>
				`;
			</c:forEach>

		} else if(lenCorsi > 0 && lenAule > 0) {
			container.innerHTML += `
				<jsp:include page="./contentView/contentViewBlank.jsp" >
					<jsp:param name="title" value="Nessuna lezione trovata" />
					<jsp:param name="text" value="Cerca in alto a destra o aggiungi una nuova lezione." />
				</jsp:include>
			`;
		} else {
			container.innerHTML += `
				<jsp:include page="./contentView/contentViewBlank.jsp" >
					<jsp:param name="title" value="Non puoi inserire una lezione" />
					<jsp:param name="text" value="Devi aver aggiunto almeno un'aula e un corso per poterla creare." />
				</jsp:include>
			`;

			var child = document.getElementById('insert-lezione');
			var parent = child.parentNode;

			var newChild = document.createElement("div");
			newChild.style.height = "50px";
			
			parent.replaceChild(newChild, child);

		}

		var header = document.getElementById('header');
		header.lastElementChild.innerHTML += `
			<jsp:include page="./searchBar/searchBar.jsp" >
				<jsp:param name="link" value="find-lezioni" />
			</jsp:include>
		`;

	</script>
</html>