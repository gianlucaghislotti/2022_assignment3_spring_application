package com.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.demo.dao.AulaDao;
import com.demo.model.Aula;

@Component
@Service
public class AulaServices {

	@Autowired
	AulaDao aulaDao;
	
	
	
	//add aula
	public void addAula(Aula aula)
	{
		aulaDao.addAula(aula);
	}
	
	//get all aule
	public List<Aula> getAllAule()
	{
		return aulaDao.getAllAule();
	}
	
	
	//get aula by id
	
	public Aula getAulaById(int id)
	{
		return aulaDao.getAulaById(id);
	}
	
	
	// update aula
	
	public void updateAula(Aula aula)
	{
		aulaDao.updateAula(aula);
	}
	
	
	//delete aula 
	
	public void deleteAula(int id)
	{
		aulaDao.deleteAula(id);
	}
}
