package com.demo.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.demo.dao.CorsoDao;
import com.demo.model.Corso;

@Component
@Service
public class CorsoServices {

    @Autowired
	CorsoDao corsoDao;
	

	public void addCorso(Corso corso)
	{
		corsoDao.addCorso(corso);
	}

	
	public List<Corso> getAllCorsi()
	{
		return corsoDao.getAllCorsi();
	}
	

	public Corso getCorsoById(int idC)
	{
		return corsoDao.getCorsoById(idC);
	}


	public void updateCorso(Corso corso)
	{
		corsoDao.updateCorso(corso);
	}
	
    
	public void deleteCorso(int idC)
	{
		corsoDao.deleteCorso(idC);
	}


	public void addPropedeuticoA(Corso c1, Corso c2){
		corsoDao.addPropedeuticoA(c1, c2);
	}

	public void removePropedeuticoA(Corso c1, Corso c2){
		corsoDao.removePropedeuticoA(c1, c2);
	}

	public void addPropedeutici(Corso c1, Corso c2){
		corsoDao.addPropedeutici(c1, c2);
	}

	public void removePropedeutici(Corso c1, Corso c2){
		corsoDao.removePropedeutici(c1, c2);
	}

	public void updatePropedeuticoA(Corso c1, Set<Corso>c2){
		corsoDao.updatePropedeuticoA(c1, c2);
	}

	public void updatePropedeutici(Corso c1, Set<Corso>c2){
		corsoDao.updatePropedeutici(c1, c2);
	}
	
	public List<Corso> getAllPropedeuticoA(Corso c1){
		return corsoDao.getAllPropedeuticoA(c1);
	}
	
	public List<Corso> getAllPropedeutici(Corso c1){
		return corsoDao.getAllPropedeutici(c1);
	}

	public List<Corso> getPossPropedeutici(Corso c1){
		return corsoDao.getPossPropedeutici(c1);
	}
	
	public void deleteAllPropedeutici(Corso c1) {
		corsoDao.deleteAllPropedeutici(c1);
	}

	public List<Corso> getCorsiConPropedeutico(Corso corsoModificato){
		return corsoDao.getCorsiConPropedeutico(corsoModificato);
	}

	public void updatePropedeuticiCorsi(List<Corso> cList, Corso c){
		corsoDao.updatePropedeuticiCorsi(cList, c);
	}
}
