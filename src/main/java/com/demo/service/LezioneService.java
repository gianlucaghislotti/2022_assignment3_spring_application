package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.demo.dao.LezioneDao;
import com.demo.model.Lezione;

@Component
@Service
public class LezioneService {
	@Autowired
	LezioneDao lezioneDao;
	
	
	
	//add aula
	public void addLezione(Lezione lezione)
	{
		lezioneDao.addLezione(lezione);
	}
	
	//get all aule
//	public List<Teoria> getAllLezioniTeoria()
//	{
//		return lezioneDao.getAllLezioniTeoria();
//	}
//	
//	public List<Laboratorio> getAllLezioniLaboratorio()
//	{
//		return lezioneDao.getAllLezioniLaboratorio();
//	}
	
	public List<Lezione> getAllLezioni()
	{
		return lezioneDao.getAllLezioni();
	}
	
	
	//get aula by id
	
	public Lezione getLezioneById(int id)
	{
		return lezioneDao.getLezioneById(id);
	}
	
	
	// update aula
	
	public void updateLezione(Lezione lezione)
	{
		lezioneDao.updateLezione(lezione);
	}
	
	
	//delete aula 
	
	public void deleteLezione(int id)
	{
		lezioneDao.deleteLezione(id);
	}

	public List<Lezione> findLezione(String adC, int aC, String csC){
		return lezioneDao.findLezione(adC, aC, csC);
	}
}
