package com.demo.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.demo.dao.LezioneDao;
import com.demo.model.Lezione;

@Component
public class LezioneDaoImpl implements LezioneDao{

	@Autowired
	HibernateTemplate hiberneteTemplate;
	
	
	//add lezione
	@Transactional
	@Override
	public void addLezione(Lezione lezione)
	{
		hiberneteTemplate.save(lezione);
	}
	
	
	//get all lezioni
//	public List<Teoria> getAllLezioniTeoria()
//	{
//
//		return hiberneteTemplate.loadAll(Teoria.class);
//	}
//	
//	public List<Laboratorio> getAllLezioniLaboratorio()
//	{
//		return hiberneteTemplate.loadAll(Laboratorio.class);
//	}
	
	@Override
	@Transactional
	public List<Lezione> getAllLezioni()
	{
//		Session session = hiberneteTemplate.getSessionFactory().openSession();
//	    TypedQuery<Lezione> query = session.createQuery("Select l.class FROM Lezione l");
//	    List<Lezione> result = query.getResultList();
//	    session.close();
//	    return result;
		return hiberneteTemplate.loadAll(Lezione.class);
	}
	
	//get Lezione by id
	@Transactional
	@Override
	public Lezione getLezioneById(int id)
	{
		
		Lezione lezione= hiberneteTemplate.get(Lezione.class, id);
		return lezione;
	}
	
	
	//update lezione
	@Transactional
	@Override
	public void updateLezione(Lezione lezione)
	{
		hiberneteTemplate.update(lezione);
	}
	
	
	//delete lezione
	@Transactional
	@Override
	public void deleteLezione(int id)
	{
		hiberneteTemplate.delete(hiberneteTemplate.load(Lezione.class, id));
	}

	@Transactional
	@Override
	public List<Lezione> findLezione(String tC, int aC, String csC){

		Session session = hiberneteTemplate.getSessionFactory().openSession();
	    TypedQuery<Lezione> query = session.createQuery("Select l FROM Lezione l JOIN l.codC c WHERE c.tipoC=:tC AND c.annoC=:aC AND c.corso_studioC=:csC");
	    query.setParameter("tC", tC);
		query.setParameter("aC", aC);
		query.setParameter("csC", csC);
		List<Lezione> result = query.getResultList();
	    session.close();


		return result;
	}
}
