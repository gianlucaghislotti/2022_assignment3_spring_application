package com.demo.daoImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;


import com.demo.model.Aula;
import com.demo.dao.AulaDao;

@Component
public class AulaDaoImpl implements AulaDao{

	
	@Autowired
	HibernateTemplate hiberneteTemplate;
	
	
	//add aula
	@Transactional
	@Override
	public void addAula(Aula aula)
	{
		hiberneteTemplate.save(aula);
	}
	
	
	//get all aula
	@Override
	public List<Aula> getAllAule()
	{
		return hiberneteTemplate.loadAll(Aula.class);
	}
	
	//get aula by id
	@Transactional
	@Override
	public Aula getAulaById(int id)
	{
		
		Aula aula= hiberneteTemplate.get(Aula.class, id);
		return aula;
	}
	
	
	//update aula
	@Transactional
	@Override
	public void updateAula(Aula aula)
	{
		hiberneteTemplate.update(aula);
	}
	
	
	//delete aula
	@Transactional
	@Override
	public void deleteAula(int id)
	{
		hiberneteTemplate.delete(hiberneteTemplate.load(Aula.class, id));
	}
}
