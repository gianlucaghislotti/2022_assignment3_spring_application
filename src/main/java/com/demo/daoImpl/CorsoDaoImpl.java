package com.demo.daoImpl;
import java.util.List;
import java.util.ArrayList;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;


import com.demo.model.Corso;
import com.demo.dao.CorsoDao;

@Component
public class CorsoDaoImpl implements CorsoDao{
    
    @Autowired
	HibernateTemplate hiberneteTemplate;
	

	@Transactional
	@Override
	public void addCorso(Corso corso)
	{
		hiberneteTemplate.save(corso);
	}
	
    @Override
    @Transactional
	public List<Corso> getAllCorsi()
	{
		return hiberneteTemplate.loadAll(Corso.class);
	}
	

	@Transactional
	@Override
	public Corso getCorsoById(int id)
	{
		
		Corso corso= hiberneteTemplate.get(Corso.class, id);
		return corso;
	}
	

	@Transactional
	@Override
	public void updateCorso(Corso corso)
	{
		hiberneteTemplate.update(corso);
	}
	

	@Transactional
	@Override
	public void deleteCorso(int id)
	{		
		hiberneteTemplate.delete(hiberneteTemplate.load(Corso.class, id));
		
	}

	@Transactional
	@Override
	public void addPropedeuticoA(Corso c1, Corso c2)
	{
		c1.getPropedeuticoA().add(c2);
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public void removePropedeuticoA(Corso c1, Corso c2)
	{
		c1.getPropedeuticoA().remove(c2);
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public void addPropedeutici(Corso c1, Corso c2)
	{
		c1.getPropedeutici().add(c2);
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public void removePropedeutici(Corso c1, Corso c2)
	{
		c1.getPropedeutici().remove(c2);
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public void updatePropedeuticoA(Corso c1, Set<Corso> c2){
		c1.setPropedeuticoA(c2);
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public void updatePropedeutici(Corso c1, Set<Corso> c2){
		c1.setPropedeutici(c2);
		hiberneteTemplate.update(c1);
	}
	
	@Transactional
	@Override
	public List<Corso> getAllPropedeuticoA(Corso c1){
		return new ArrayList<Corso>(c1.getPropedeuticoA());
	}
	
	@Transactional
	@Override
	public List<Corso> getAllPropedeutici(Corso c1){
		return new ArrayList<Corso>(c1.getPropedeutici());
	}
	
	@Transactional
	@Override
	public void deleteAllPropedeutici(Corso c1) {
		c1.deletePropedeutici();
		c1.deletePropedeuticoA();
		hiberneteTemplate.update(c1);
	}

	@Transactional
	@Override
	public List<Corso> getPossPropedeutici(Corso c){
		
		List<Corso> corsi = getAllCorsi();

		for(int i=0; i<corsi.size(); i++) {
			if(c.isPropedeuticoDi(corsi.get(i)) == true || corsi.get(i).getIdC() == c.getIdC()) {
				corsi.remove(i);
				i -= 1;
			}
		}
		
		return corsi;
	}
 
	@Transactional 
	@Override
	public List<Corso> getCorsiConPropedeutico(Corso corsoModificato){
		List<Corso> corsi = new ArrayList<Corso>(getAllCorsi());
		List<Corso> corsiDaAggiornare = new ArrayList<Corso>();

		for(int i=0; i<corsi.size(); i++) {
			if(corsoModificato.isPropedeuticoDi(corsi.get(i))) {
				corsiDaAggiornare.add(corsi.get(i));
			}
		}

		return corsiDaAggiornare;
	}

	@Transactional
	@Override
	public void updatePropedeuticiCorsi(List<Corso> cList, Corso c){
		
		for(int i=0; i< cList.size(); i++){
			addPropedeutici(cList.get(i), c);
		}
	}
}
