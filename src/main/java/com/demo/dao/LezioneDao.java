package com.demo.dao;

import java.util.List;

import com.demo.model.Lezione;

public interface LezioneDao {
    
    public void addLezione(Lezione lezione);
    public List<Lezione> getAllLezioni();
    public Lezione getLezioneById(int id);
    public void updateLezione(Lezione lezione);
    public void deleteLezione(int id);
    public List<Lezione> findLezione(String tC, int aC, String csC);
}
