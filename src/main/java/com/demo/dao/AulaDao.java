package com.demo.dao;

import com.demo.model.Aula;
import java.util.List;

public interface AulaDao {
    

    public void addAula(Aula aula);
    public List<Aula> getAllAule();
    public Aula getAulaById(int id);
    public void updateAula(Aula aula);
    public void deleteAula(int id);
}
