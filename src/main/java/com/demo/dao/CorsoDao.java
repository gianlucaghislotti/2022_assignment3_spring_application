package com.demo.dao;

import java.util.List;
import java.util.Set;

import com.demo.model.Corso;

public interface CorsoDao {
    
    public void addCorso(Corso corso);
    public List<Corso> getAllCorsi();
    public Corso getCorsoById(int id);
    public void updateCorso(Corso corso);
    public void deleteCorso(int id);
    public void addPropedeuticoA(Corso c1, Corso c2);
    public void removePropedeuticoA(Corso c1, Corso c2);
    public void addPropedeutici(Corso c1, Corso c2);
    public void removePropedeutici(Corso c1, Corso c2);
    public void updatePropedeuticoA(Corso c1, Set<Corso> c2);
    public void updatePropedeutici(Corso c1, Set<Corso> c2);
    public List<Corso> getAllPropedeuticoA(Corso c1);
    public List<Corso> getAllPropedeutici(Corso c1);
    public List<Corso> getPossPropedeutici(Corso c1);
    public void deleteAllPropedeutici(Corso c1);
    public List<Corso> getCorsiConPropedeutico(Corso corsoModificato);
    public void updatePropedeuticiCorsi(List<Corso> cList, Corso c);
}
