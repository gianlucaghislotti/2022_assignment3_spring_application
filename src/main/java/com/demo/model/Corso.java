package com.demo.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.transaction.Transactional;

@Transactional
@Entity
@Table
public class Corso{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idC; 

    @Column(length = 100, nullable = false)
    private String nomeC; 

    @Column(nullable = false)
    private int annoC;

    @Column(length = 50, nullable = false)
    private String area_didatticaC; 

    @Column(length = 50, nullable = false)
    private String corso_studioC;

    @Column(nullable = false)
    private int semestreC;

	@Column(length = 70, nullable = false)
    private String tipoC;
    
    @Column(length = 50, nullable = false)
    private String linguaC;

	@Column(length = 50, nullable = false)
	private String settoreC;

	@Column(length = 50, nullable = false)
	private String attivitaC;

	@Column(nullable = false)
	private int CFU;
	
	@OneToMany(mappedBy="codC", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<Lezione> setLezioni = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(		joinColumns={@JoinColumn(name="idCP", nullable = true)}, 
					inverseJoinColumns={@JoinColumn(name="idCA", nullable = true)})
	private Set<Corso> propedeuticoA = new HashSet<Corso>();

	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable( 	joinColumns={@JoinColumn(name="idCA", nullable = true)}, 
					inverseJoinColumns={@JoinColumn(name="idCP", nullable = true)})
	private Set<Corso> propedeutici = new HashSet<Corso>();

	public Corso(String nomeC, int annoC, String area_didatticaC, String corso, int semestreC,
			String tipoC, String linguaC, String settoreC, String attivitaC, int cfu) {
		super();
		this.nomeC = nomeC;
		this.annoC = annoC;
		this.area_didatticaC = area_didatticaC;
		this.corso_studioC = corso;
		this.semestreC = semestreC;
		this.tipoC = tipoC;
		this.linguaC = linguaC;
		this.settoreC = settoreC;
		this.attivitaC = attivitaC;
		CFU = cfu;
	}
	public Corso(){}


	public int getIdC() {
		return idC;
	}

	public void setIdC(int idC) {
		this.idC = idC;
	}

	public String getNomeC() {
		return nomeC;
	}

	public void setNomeC(String nomeC) {
		this.nomeC = nomeC;
	}

	public int getAnnoC() {
		return annoC;
	}

	public void setAnnoC(int annoC) {
		this.annoC = annoC;
	}

	public String getArea_didatticaC() {
		return area_didatticaC;
	}

	public void setArea_didatticaC(String area_didatticaC) {
		this.area_didatticaC = area_didatticaC;
	}

	public String getCorso_studioC() {
		return corso_studioC;
	}

	public void setCorso_studioC(String corso_studioC) {
		this.corso_studioC = corso_studioC;
	}

	public int getSemestreC() {
		return semestreC;
	}

	public void setSemestreC(int semestreC) {
		this.semestreC = semestreC;
	} 

	public String getLinguaC() {
		return linguaC;
	}

	public void setLinguaC(String linguaC) {
		this.linguaC = linguaC;
	} 

	public String getSettoreC() {
		return settoreC;
	}

	public void setSettoreC(String settoreC) {
		this.settoreC = settoreC;
	} 

	public String getAttivitaC() {
		return attivitaC;
	}

	public void setAttivitaC(String attivitaC) {
		this.attivitaC = attivitaC;
	} 

	public String getTipoC() {
		return tipoC;
	}

	public void setTipoC(String tipoC) {
		this.tipoC = tipoC;
	}

	public int getCFU() {
		return CFU;
	}

	public void setCFU(int CFU) {
		this.CFU = CFU;
	}

	public Set<Corso> getPropedeutici() {
		return propedeutici;
	}

	public void setPropedeutici(Set<Corso> propedeuticoP) {
		this.propedeutici = propedeuticoP;
	}

	public Set<Corso> getPropedeuticoA() {
		return propedeuticoA;
	}

	public void setPropedeuticoA(Set<Corso> propedeuticoA) {
		this.propedeuticoA = propedeuticoA;
	} 
	
	public void deletePropedeuticoA() {
		this.propedeuticoA.clear();
	}
	
	public void deletePropedeutici() {
		this.propedeutici.clear();
	}

	public boolean isPropedeuticoDi(Corso c) {
		
		List<Corso> propedeutici = new ArrayList<Corso>(c.propedeutici);
		
		for(int i=0; i<c.propedeutici.size(); i++) {
			if(propedeutici.get(i).getIdC() == this.getIdC()) {
				return true;
			}
		}
		
		return false;
	}
	
	public List<Lezione> getSetLezioni() {
		return setLezioni;
	}

	public void setSetLezioni(List<Lezione> setLezioni) {
		this.setLezioni = setLezioni;
	}
	
}
