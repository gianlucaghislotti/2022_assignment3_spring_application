package com.demo.model;


import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;

import lombok.Data;

@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "LEZIONE_TYPE")
public class Lezione{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idL;
	
	@Column(length=500, nullable = false)
	private String descrizioneL;
	
	@Column(length=10, nullable = false)
	private String dataL;
	
	@Column(length=10, nullable = false)
	private String ora_inizioL;
	
	@Column(length=10, nullable = false)
	private String ora_fineL;
	
	
	@ManyToOne
	@JoinColumn(name = "codA", nullable = false)
	private Aula codA;
	
	
	@ManyToOne
	@JoinColumn(name = "codC", nullable = false)
	private Corso codC;
	
	public Lezione(){}
	
	public Lezione(String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA,
			Corso codC) {
		this.descrizioneL = descrizioneL;
		this.dataL = dataL;
		this.ora_inizioL = ora_inizioL;
		this.ora_fineL = ora_fineL;
		this.codA = codA;
		this.codC = codC;
	}
	
	// Constructor with id
	public Lezione(int idL, String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA,
			Corso codC) {
		this.idL = idL;
		this.descrizioneL = descrizioneL;
		this.dataL = dataL;
		this.ora_inizioL = ora_inizioL;
		this.ora_fineL = ora_fineL;
		this.codA = codA;
		this.codC = codC;
	}
	

	public int getIdL() {
		return idL;
	}


	public void setIdL(int idL) {
		this.idL = idL;
	}

	public String getDescrizioneL() {
		return descrizioneL;
	}

	public void setDescrizioneL(String descrizioneL) {
		this.descrizioneL = descrizioneL;
	}

	public String getDataL() {
		return dataL;
	}

	public void setDataL(String dataL) {
		this.dataL = dataL;
	}

	public String getOra_inizioL() {
		return ora_inizioL;
	}

	public void setOra_inizioL(String ora_inizioL) {
		this.ora_inizioL = ora_inizioL;
	}

	public String getOra_fineL() {
		return ora_fineL;
	}

	public void setOra_fineL(String ora_fineL) {
		this.ora_fineL = ora_fineL;
	}

	public Aula getCodA() {
		return codA;
	}

	public void setCodA(Aula codA) {
		this.codA = codA;
	}

	public Corso getCodC() {
		return codC;
	}

	public void setCodC(Corso codC) {
		this.codC = codC;
	}


}
