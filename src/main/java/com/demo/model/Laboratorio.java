package com.demo.model;


import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("laboratorio") 
public class Laboratorio extends Lezione{
	
	@Column(nullable = true)
	private Boolean computer;
	
	@Column(length=30, nullable = true)
	private String materiale;

	public Laboratorio() {
		super();
	}

	public Laboratorio(String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA, Corso codC,
			Boolean computer, String materiale) {
		super(descrizioneL, dataL, ora_inizioL, ora_fineL, codA, codC);
		this.computer = computer;
		this.materiale = materiale;
	}
	// Constructor with id
	public Laboratorio(int idL, String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA, Corso codC,
			Boolean computer, String materiale) {
		super(idL, descrizioneL, dataL, ora_inizioL, ora_fineL, codA, codC);
		this.computer = computer;
		this.materiale = materiale;
	}
	
	public Boolean getComputer() {
		return computer;
	}

	public void setComputer(Boolean computer) {
		this.computer = computer;
	}

	public String getMateriale() {
		return materiale;
	}

	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}


	


	

}
