package com.demo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Aula {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idA;
	
	@Column(length = 100, nullable = false)
	private String nomeA;
	
	@Column(nullable = false)
	private int postiA;
	
	@OneToMany(mappedBy="codA", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
	private List<Lezione> setAule = new ArrayList<>();

	public int getIdA() {
		return idA;
	}

	public void setIdA(int idA) {
		this.idA = idA;
	}

	public String getNomeA() {
		return nomeA;
	}

	public void setNomeA(String nomeA) {
		this.nomeA = nomeA;
	}

	public int getPostiA() {
		return postiA;
	}

	public void setPostiA(int postiA) {
		this.postiA = postiA;
	}
	
	public List<Lezione> getSetAule() {
		return setAule;
	}

	public void setSetAule(List<Lezione> setAule) {
		this.setAule = setAule;
	}
	
	
}