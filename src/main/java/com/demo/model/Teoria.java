package com.demo.model;


import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("teoria")  
public class Teoria extends Lezione{

	@Column(length=20, nullable = true)
	private String libro_testo;
	
	@Column(nullable = true)
	private Boolean tutor;


	public Teoria() {
		super();
	}
	
	public Teoria(String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA, Corso codC,
			String libro_testo, Boolean tutor) {
		super(descrizioneL, dataL, ora_inizioL, ora_fineL, codA, codC);
		this.libro_testo = libro_testo;
		this.tutor = tutor;
	}
	
	// Constructor with id
	public Teoria(int idL, String descrizioneL, String dataL, String ora_inizioL, String ora_fineL, Aula codA, Corso codC,
			String libro_testo, Boolean tutor) {
		super(idL, descrizioneL, dataL, ora_inizioL, ora_fineL, codA, codC);
		this.libro_testo = libro_testo;
		this.tutor = tutor;
	}

	public String getLibro_testo() {
		return libro_testo;
	}

	public void setLibro_testo(String libro_testo) {
		this.libro_testo = libro_testo;
	}

	public Boolean getTutor() {
		return tutor;
	}

	public void setTutor(Boolean tutor) {
		this.tutor = tutor;
	}
	
	

}

