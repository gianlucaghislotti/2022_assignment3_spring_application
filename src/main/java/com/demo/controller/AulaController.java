package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.demo.model.Aula;
import com.demo.service.AulaServices;

@Controller
public class AulaController {
	
	
	@Autowired
	AulaServices aulaServices;
	

	@RequestMapping(value="/add-aula", method = RequestMethod.POST)
	public String addAula(@ModelAttribute("add-aula") Aula aula)
	{
		aula.setNomeA(aula.getNomeA().toLowerCase());
		aulaServices.addAula(aula);
		return "redirect:/aule";
	}


	@RequestMapping(value="/insert-aula", method = RequestMethod.GET)
	public String insertAula(Model m)
	{
		return "insert-aula";
	}
	
	@RequestMapping(value="/aule", method = RequestMethod.GET)
	public String lodeAula(Model m)
	{
		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("aule", aule);
		m.addAttribute("len", aule.size());
		return "aule";
	}
 
	@RequestMapping(value="/view-aula/{id}", method = RequestMethod.GET)
	public String lodeEditForm(Model m, @PathVariable(value="id") int id)
	{
		m.addAttribute("aula", aulaServices.getAulaById(id));
		return "update-aula";
		
	}

	@RequestMapping(value="/update-aula/{id}", method = RequestMethod.POST)
	public String updateAula(@ModelAttribute("update-aula/{id}") Aula aula, 
							 @PathVariable(value="id") int id)
	{
		aula.setNomeA(aula.getNomeA().toLowerCase());
		aula.setIdA(id);
		aulaServices.updateAula(aula);
		return "redirect:/aule";
	}

	@RequestMapping(value="/delete-aula/{id}", method = RequestMethod.POST)
	public String deleteAula(@PathVariable int id)
	{
		aulaServices.deleteAula(id);
		return "redirect:/aule";
	}
}
