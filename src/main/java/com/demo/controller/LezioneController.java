package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.model.Lezione;
import com.demo.model.Teoria;
import com.demo.model.Aula;
import com.demo.model.Corso;
import com.demo.model.Laboratorio;
import com.demo.service.AulaServices;
import com.demo.service.CorsoServices;
import com.demo.service.LezioneService;
import com.google.gson.JsonObject;



@Controller
public class LezioneController {
	
	
	@Autowired
	LezioneService lezioneService;
	@Autowired
	AulaServices aulaServices;
	@Autowired
	CorsoServices corsoServices; 
	

	@RequestMapping(value="/add-lezione", method = RequestMethod.POST)
	public String addLezione(
			@RequestParam String tipo, 
			@RequestParam int aula, 
			@RequestParam int corso,
			@RequestParam String ora_inizio,
			@RequestParam String ora_fine,
			@RequestParam String descrizione,
			@RequestParam String giorno,
			@RequestParam(required=false) Boolean computerL,
			@RequestParam (required=false)String materialeL,
			@RequestParam(required=false) String libro_testoT,
			@RequestParam (required=false) Boolean tutorT
			)
	{
		Corso codC = corsoServices.getCorsoById(corso);
		Aula codA = aulaServices.getAulaById(aula);
		switch(tipo) {
			case "laboratorio":
					Laboratorio laboratorio = new Laboratorio(
							descrizione.toLowerCase(),
							giorno.toLowerCase(),
							ora_inizio,
							ora_fine,
							codA,
							codC,
							computerL,
							materialeL.toLowerCase()
							);
					lezioneService.addLezione(laboratorio);
				break;
			case "teoria":
					Teoria teoria = new Teoria(
							descrizione.toLowerCase(),
							giorno.toLowerCase(),
							ora_inizio,
							ora_fine,
							codA,
							codC,
							libro_testoT.toLowerCase(),
							tutorT
							);
					lezioneService.addLezione(teoria);
				break;
		}
		return "redirect:/lezioni";
	}

	@RequestMapping(value="/insert-lezione", method = RequestMethod.GET)
	public String insertLezione(Model m)
	{
		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("aule", aule);
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("corsi", corsi);
		
		return "insert-lezione";
	}

	@RequestMapping(value="/lezioni", method = RequestMethod.GET)
	public String lodeLezioni(Model m)
	{
		
		List<Lezione> lezioni = lezioneService.getAllLezioni();
		m.addAttribute("lezioni", lezioni);
		m.addAttribute("len", lezioni.size());

		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("lenAule", aule.size());
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("lenCorsi", corsi.size());
		
		return "lezioni";
		
	}

	@RequestMapping(value="/view-lezione/{id}", method = RequestMethod.GET)
	public String lodeEditForm(Model m, @PathVariable(value="id") int id)
	{

		Lezione lezione = lezioneService.getLezioneById(id);
		m.addAttribute("lezione", lezione);
		
		JsonObject innerObject = new JsonObject();
		if(lezione instanceof Teoria){
			 Teoria teo = (Teoria) lezione;
			 innerObject.addProperty("libro_testo", teo.getLibro_testo());
			 innerObject.addProperty("tutor", teo.getTutor());
		 }else {
			 Laboratorio lab = (Laboratorio) lezione;
			 innerObject.addProperty("materiale", lab.getMateriale());
			 innerObject.addProperty("computer", lab.getComputer());
		 }
        m.addAttribute("lezioneJson", innerObject);
		
		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("aule", aule);
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("corsi", corsi);

		return "update-lezione";

		
	}

	@RequestMapping(value="/update-lezione/{id}", method = RequestMethod.POST)
	public String updateLezione(
				@PathVariable(value="id") int id,
				@RequestParam String tipo, 
				@RequestParam int aula, 
				@RequestParam int corso,
				@RequestParam String ora_inizio,
				@RequestParam String ora_fine,
				@RequestParam String descrizione,
				@RequestParam String giorno,
				@RequestParam(required=false) Boolean computerL,
				@RequestParam (required=false)String materialeL,
				@RequestParam(required=false) String libro_testoT,
				@RequestParam (required=false) Boolean tutorT
				)
	{
		Corso codC = corsoServices.getCorsoById(corso);
		Aula codA = aulaServices.getAulaById(aula);
		System.out.println(materialeL);
		switch(tipo) {
			case "laboratorio":
					Laboratorio laboratorio = new Laboratorio(
							id,
							descrizione.toLowerCase(),
							giorno.toLowerCase(),
							ora_inizio,
							ora_fine,
							codA,
							codC,
							computerL,
							materialeL.toLowerCase()
							);
					lezioneService.updateLezione(laboratorio);
				break;
			case "teoria":
					Teoria teoria = new Teoria(
							id,
							descrizione.toLowerCase(),
							giorno.toLowerCase(),
							ora_inizio,
							ora_fine,
							codA,
							codC,
							libro_testoT.toLowerCase(),
							tutorT
							);
					lezioneService.updateLezione(teoria);
				break;
		}

		return "redirect:/lezioni";
	}
	
	@RequestMapping(value="/delete-lezione/{id}", method = RequestMethod.POST)
	public String deleteLezione(@PathVariable int id)
	{
		lezioneService.deleteLezione(id);
		return "redirect:/lezioni";
	}
 
	/*
	@RequestMapping(value="/find-lezioni/{tipo}/{anno}/{corsoStudi}", method = RequestMethod.GET)
	public String findLezione(Model m, @PathVariable String tipo, 
							  @PathVariable int anno, 
							  @PathVariable String corsoStudi)
	{
		System.out.println("\n\n");
		System.out.println(tipo);
		List<Lezione> lezioni = lezioneService.findLezione(tipo, anno, corsoStudi);
		
		m.addAttribute("lezioni", lezioni);
		m.addAttribute("len", lezioni.size());

		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("lenAule", aule.size());
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("lenCorsi", corsi.size());
		
		return "lezioni";
	}
	*/

	@RequestMapping(value="/find-lezioni", method = RequestMethod.GET)
	public String findLezione(Model m, @RequestParam String tipo, 
							  @RequestParam int anno, 
							  @RequestParam String corsoStudi)
	{
		List<Lezione> lezioni = lezioneService.findLezione(tipo.toLowerCase(), anno, corsoStudi.toLowerCase());
		
		m.addAttribute("lezioni", lezioni);
		m.addAttribute("len", lezioni.size());

		List<Aula> aule = aulaServices.getAllAule();
		m.addAttribute("lenAule", aule.size());
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("lenCorsi", corsi.size());
		
		return "lezioni";
	}

}
