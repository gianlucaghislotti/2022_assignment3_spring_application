package com.demo.controller;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.model.Corso;
import com.demo.service.CorsoServices;

@Controller
public class CorsoController {
	
	
	@Autowired
	CorsoServices corsoServices;

	@RequestMapping(value="/add-corso", method = RequestMethod.POST)
	public String addCorso(
				@RequestParam String nome, 
				@RequestParam String tipo, 
				@RequestParam String area, 
            	@RequestParam String corso, 
				@RequestParam int anno, 
				@RequestParam String lingua,
            	@RequestParam String attivita, 
				@RequestParam String settore, 
				@RequestParam int semestre,
            	@RequestParam int cfu,
				@RequestParam List<Integer> propedeutici)
	{
		
		Corso c =  new Corso(nome.toLowerCase(), 
						     anno, 
							 area.toLowerCase(), 
							 corso.toLowerCase(), 
							 semestre, 
							 tipo.toLowerCase(), 
							 lingua.toLowerCase(), 
							 settore.toLowerCase(), 
							 attivita.toLowerCase(), 
							 cfu);

		corsoServices.addCorso(c);

		for(int i=0; i<propedeutici.size(); i++){
			
			int id_propedeutico = propedeutici.get(i);
			Corso corso_propedeutico = corsoServices.getCorsoById(id_propedeutico);
			corsoServices.addPropedeutici(c, corso_propedeutico);
		}

		return "redirect:/corsi";
	}

	@RequestMapping(value="/insert-corso", method = RequestMethod.GET)
	public String insertCorso(Model m)
	{
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("corsi", corsi);
		m.addAttribute("len", corsi.size());

		return "insert-corso";
	}
	
	@RequestMapping(value="/corsi", method = RequestMethod.GET)
	public String lodeCorsi(Model m)
	{
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("corsi", corsi);
		m.addAttribute("len", corsi.size());

		return "corsi";
	} 

	@RequestMapping(value="/view-corso/{id}", method = RequestMethod.GET)
	public String lodeEditForm(Model m, @PathVariable(value="id") int id)
	{
		Corso corso = corsoServices.getCorsoById(id);
		m.addAttribute("corso", corso);
		
		List<Corso> corsi = corsoServices.getAllCorsi();
		m.addAttribute("corsi", corsi);
		
		List<Corso> propedeutici = corsoServices.getAllPropedeutici(corso);
		m.addAttribute("propedeutici", propedeutici);
		
		List<Corso> possPropedeutici = corsoServices.getPossPropedeutici(corso);
		m.addAttribute("possPropedeutici", possPropedeutici);
		m.addAttribute("len", possPropedeutici.size());

		return "update-corso";
	}

	@RequestMapping(value="/update-corso/{id}", method = RequestMethod.POST)
	public String updateCorso(
					@RequestParam String nome, 
					@RequestParam String tipo, 
					@RequestParam String area, 
					@RequestParam String corso, 
					@RequestParam int anno, 
					@RequestParam String lingua,
					@RequestParam String attivita, 
					@RequestParam String settore, 
					@RequestParam int semestre,
					@RequestParam int cfu, 
					@RequestParam List<Integer> propedeutici,
					@PathVariable(value="id") int id)
	{
		Corso corsoModificato = new Corso(nome.toLowerCase(), 
										  anno, 
										  area.toLowerCase(), 
										  corso.toLowerCase(), 
										  semestre, 
										  tipo.toLowerCase(), 
										  lingua.toLowerCase(), 
										  settore.toLowerCase(), 
										  attivita.toLowerCase(), 
										  cfu);
		corsoModificato.setIdC(id);

		List<Corso> corsiDaAggiornare = new ArrayList<Corso>(corsoServices.getCorsiConPropedeutico(corsoModificato));
		List<Corso> myList = new ArrayList<Corso>();

		for(int i=0; i<propedeutici.size(); i++){
			
			int id_propedeutico = propedeutici.get(i);
			Corso corso_propedeutico = corsoServices.getCorsoById(id_propedeutico);
			myList.add(corso_propedeutico);
		}

		HashSet<Corso> listaPropedeutici = new HashSet<Corso>(myList);
		corsoServices.updatePropedeutici(corsoModificato, listaPropedeutici);
		corsoServices.updateCorso(corsoModificato);
		corsoServices.updatePropedeuticiCorsi(corsiDaAggiornare, corsoModificato);

		return "redirect:/corsi";
	}

	@RequestMapping(value="/delete-corso/{id}", method = RequestMethod.POST)
	public String deleteCorso(@PathVariable int id)
	{
		corsoServices.deleteAllPropedeutici(corsoServices.getCorsoById(id));
		corsoServices.deleteCorso(id);
		return "redirect:/corsi";
	}

	@RequestMapping(value="/updatePropedeuticoA", method = RequestMethod.POST)
	public void updatePropedeuticoA(@RequestBody Corso c1, @RequestBody List<Corso> c2){

		corsoServices.updatePropedeuticoA(c1, new HashSet<Corso>(c2));
	}
}
