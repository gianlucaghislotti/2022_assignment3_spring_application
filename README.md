# 2022 Third Assignment - Spring Application

## Installation Required

* Docker (https://www.docker.com/products/docker-desktop/)
* Git (https://git-scm.com/downloads)
* Tomcat (https://tomcat.apache.org/download-90.cgi)
* Eclipse (https://www.eclipse.org/downloads/)

## How to run it

### Clone the project

            git clone https://gitlab.com/gianlucaghislotti/2022_assignment3_spring_application.git

### Start the database container
On a terminal (cmd), go to the same level of the *docker-compose.yml* file, inside the project repository, type:

            docker-compose up

### Run the project
* **Open Eclipse**
* **Import the project**:<br>
File -> Import... -> Maven -> **Existing Maven Projects** -> *Root directory*: select the project repository -> Finish
* **Start the project server**:<br>
On the left side in Project Explorer, Right click on the project directory -> Run As -> Run on a server -> Check: *Manually define a new server* -> Apache -> Tomcat vX.x Server (it depends on the version you have downloaded) -> select *SpringMvcCrud* to the right side (Configured) -> Finish.
* **Open your browser**<br>
Type the link: 
            http://localhost:8080/SpringMvcCrud/

* **Enjoy!!**
